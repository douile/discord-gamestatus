# discord-gamestatus

A discord bot that actively monitors your game server and updates a discord message with the
current status.

- [Invite link](https://discordapp.com/oauth2/authorize?client_id=659050996730822665&permissions=126144&scope=bot)
- [Command documentation](https://gamestatus.douile.com/docs/user)
- [Self-hosting documentation (WIP)](https://gamestatus.douile.com/docs/admin)
- [Terms of usage (for public bot)](https://gamestatus.douile.com/TERMS)
- [Privacy policy (for public bot)](https://gamestatus.douile.com/PRIVACY)

## Contributing
TODO: Put contributing guidelines here

## LICENSE
[Licensed under GPL-3.0](./LICENSE)
